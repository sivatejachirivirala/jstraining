(function () {

  var paused = true;
  var timerId;
  this.scoreCard = function () {

    }

  //public methods

  //change the value of score and add a lap
  scoreCard.prototype.increaseScore = function (type) {
    let score;
    let teamName;
    if (type === 'Home') {
      score = document.getElementById('scorehome');
      teamName = document.getElementById('teamonename');
    } else {
      score = document.getElementById('scoreaway');
      teamName = document.getElementById('teamtwoname');
    }
    score.innerHTML = Number(score.innerHTML) + 1;
    addRecord(score, teamName);
  }

  //public method to start timer
  scoreCard.prototype.timer = function (time) {
    startTimer(time);
  }
  
  //shows the different time options
  scoreCard.prototype.showTimes = function () {
    document.getElementById("selecttime").style.opacity = 1;
  }
  
  //hides the different time options
  scoreCard.prototype.hideTimes = function () {
    document.getElementById("selecttime").style.opacity = 0;
  }
  
  //reset the timer
  scoreCard.prototype.home = function () {
    clearInterval(timerId);
    document.getElementById('time').innerHTML = '00:00'; 
    document.getElementById('home').className = 'one glow-button';
  }
  
  //resets score of home and visiting teams, timer and laps
  scoreCard.prototype.reset = function () {
    clearInterval(timerId);
    document.getElementById('scorehome').innerHTML = '00';
    document.getElementById('scoreaway').innerHTML = '00';
    document.getElementById('time').innerHTML = '00:00';
    tableRows = document.getElementsByClassName('tr');
    console.log(tableRows.length);
    while (tableRows[0]) {
      tableRows[0].remove();
    }
    document.getElementById('home').className = 'one glow-button';
  }
  
  //pause or resume  the timer
  scoreCard.prototype.pauseOrResume = function () {
    if (!paused) {
      pause()
    } else {
    	resume()
    }
  }
  
  //adds the elements to the body
  scoreCard.prototype.startMatch = function () {
    document.getElementById('maindiv').style.display = 'none';
    let scoreCardDiv = document.createElement('div');
    scoreCardDiv.className = 'center';
    scoreCardDiv.innerHTML = `
        <div class="container">
          <div style="padding:10px" class='top'>
            <input class="one" type="text" id="teamonename" value="Home Team"> 
            <input class="two" type="text" id="teamtwoname" value="Visiting Team "> 
          </div> 
          <br>
          <div class='middle'>
            <button class='one' id="scorehome" style="padding:50px;" >00</button>
            <button class='three' id="reset">Reset</button>
            <button class='two' style="padding:50px;" id="scoreaway">00</button>
          </div>
          <div class='bottom'>
            <br/>
            <button class='one glow-button' id="home">Home</button>
            <div class='three'>
              <span id="time" style="color:orange;">00:00</span>
              <div id="selecttime" style="opacity:0;" >
                <button id='5'>05</button>
                <button id='10'>10</button>
                <button id='15'>15</button>
                <button id='20'>20</button>
              </div>
            </div>
            <button class='two button' id="pause">Pause
          </div>
        </div>`;
    document.body.appendChild(scoreCardDiv);
    addTable();
    addListeners();
  };

  //private methods
  
  //add table to record scores
  function addTable() {
    let tableDiv = document.createElement('div');
    tableDiv.innerHTML = `
        <table id="table" border='1'>
          <tr>
            <th>Team Name</th>
            <th>Score</th>
            <th>Time</th>
          </tr>
        </table>`
    document.body.appendChild(tableDiv);
  }

  //add record to the table
  function addRecord(score, teamName) {
    let time = document.getElementById('time');
    let tableRow = document.createElement('tr');
    tableRow.className='tr';
    tableRow.innerHTML = `
        <td>`+teamName.value+`</td>
        <td>`+score.innerHTML+`</td>
        <td>`+time.innerHTML+'</td>';
    document.getElementById('table').appendChild(tableRow);
  }

  //adding events to different buttons
  function addListeners() {
    document.getElementById('scoreaway').addEventListener('click', function() {
      scorecard.increaseScore('Away');
    });
    document.getElementById('pause').addEventListener('click', function() {
      scorecard.pauseOrResume();
    });
    document.getElementById('selecttime').addEventListener('mouseover', function() {
      scorecard.showTimes();
    });
    document.getElementById('selecttime').addEventListener('mouseout', function() {
      scorecard.hideTimes();
    });
    document.getElementById('scorehome').addEventListener('click', function() {
      scorecard.increaseScore('Home');
    });
    document.getElementById('home').addEventListener('click', function() {
      scorecard.home();
     });
    document.getElementById('reset').addEventListener('click', function() {
      scorecard.reset();
    });
    document.getElementById('5').addEventListener('click', function() {
      scorecard.timer(5);
    });
    document.getElementById('10').addEventListener('click', function() {
      scorecard.timer(10);
    });
    document.getElementById('15').addEventListener('click', function() {
      scorecard.timer(15);
    });
    document.getElementById('20').addEventListener('click', function() {
      scorecard.timer(20);
    });
  }

  //resumes the timer
  function resume() {
    paused = false; 
    time = document.getElementById('time').innerHTML.split(':');
    document.getElementById('pause').className = 'two button';
    //starting the timer again
    startTimer(Number(time[0]) + (Number(time[1].slice(0,2)) / 60));
  }
  
  //pauses the timer
  function pause() {
    paused = true;
    clearInterval(timerId);
    document.getElementById('pause').className = 'two glow-button';
  }

  //starts the timer
  function startTimer(time) {
    if (typeof timerId !== undefined) {
      clearInterval(timerId);
    }
    paused = false;
    document.getElementById('pause').className = 'two button';
    document.getElementById('home').className = 'one button';
    var finalTime = new Date().getTime() + time * 60000;
    // Update the timer every 1 second
    timerId = setInterval(function() {
      var currentTime = new Date().getTime();
      var timeLeft = finalTime - currentTime;
      // Time calculations for minutes and seconds
      var minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);
      if ( seconds < 10) {
        seconds = '0' + seconds;
      }
      document.getElementById("time").innerHTML =  minutes + ":" + seconds;
      if (timeLeft < 0) {
        clearInterval(timerId);
        document.getElementById("time").innerHTML = "00:00";
        document.getElementById('home').className = 'one glow-button';
      }
    }, 1000);
  }

  return scoreCard;
})();