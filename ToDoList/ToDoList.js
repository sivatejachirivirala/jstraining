function createNewTask()
{
  document.getElementById("createnewtask").style.display = "none";
  document.getElementById("addtask").style.display = "block";
}
function loadHTML(){
  //main div to align to center
  let mainDiv = document.createElement('div');
  mainDiv.innerHTML = `
  <h1>ToDo List</h1>  
  <div id="createnewtask">
    <h4>click here to create new task</h4>
    <button onclick="createNewTask()">Create New Task
  </div>
  <div id='addtask' style="display: none;">
    <label>Task Name:</label>
    <input type="text" id="taskname" placeholder="Task Name...">
    <br>
    <label>Task Status </label>
    <input type="radio" name="type" id="pending_task"> Pending
    <input type="radio" name="type" id="completed_task"> Completed
    <br>
    <button onclick="addTask()">Add Task
  </div>
  <div id="taskpercentage" style="background-color:gray"> 
    <p >Pending Tasks: 
      <span id='pendingtaskpercent'>0%
      </span>
    </p>
    <p >Completed Tasks: 
      <span id='completedtaskpercent'>0%
      </span>
    </p> 
  </div>
  <div id="pendingtasks" style="background-color:orange">
    <label>Pending Tasks</label>
    <ul id="pending_tasks" class="list"></ul>
  </div>
  <br>
  <div id="completedtasks" style="background-color:green">
    <label>Completed Tasks</label>
    <ul id="completed_tasks" class="list"></ul>
  </div>
  `;
  mainDiv.className = 'center';
  mainDiv.id = "maindiv"
  document.body.appendChild(mainDiv);
}
function deleteTask()
{
  let task = this.parentElement.id
  this.parentElement.remove();
  modifyTaskPercentage();
  alert('Deleted Task '+ task);
}
function addTask()
{
  document.getElementById("createnewtask").style.display = "block";
  document.getElementById("addtask").style.display = "none";
  let listItem = document.createElement('li');
  let deleteButton = document.createElement('button');
  deleteButton.innerHTML = 'delete';
  let taskName = document.getElementById("taskname").value;
  listItem.id = taskName;
  listItem.innerHTML = taskName;
  listItem.appendChild(deleteButton);
  deleteButton.addEventListener('click', deleteTask);
  if( document.getElementById("pending_task").checked)
  {
    let list = document.getElementById('pending_tasks');
    list.appendChild(listItem);
    document.getElementById("pending_task").checked = false;
  }
  else
  {
    let list = document.getElementById('completed_tasks');
    list.appendChild(listItem);
    document.getElementById("completed_task").checked = false;
  }
  document.getElementById("taskname").value = '';
  modifyTaskPercentage();
  alert('Added Task '+ taskName);
}
function modifyTaskPercentage()
{
  let completedTaskPercentage;
  let pendingTaskPercentage;
  let completedTasks = numberOfTasks('completed_tasks');
  let pendingTasks = numberOfTasks('pending_tasks');
  let totalTasks = pendingTasks + completedTasks;
  if (totalTasks == 0)
  {
    completedTaskPercentage = 0;
    pendingTaskPercentage = 0;
  }
  else
  {
    pendingTaskPercentage = pendingTasks * 100 / totalTasks;
    completedTaskPercentage = completedTasks * 100 / totalTasks;
  }
  let percentage = document.getElementById('pendingtaskpercent');
  percentage.innerHTML= pendingTaskPercentage + '%';
  percentage = document.getElementById('completedtaskpercent');
  percentage.innerHTML= completedTaskPercentage + '%';
}
function numberOfTasks(id)
{
  let taskList = document.getElementById(id);
  let tasks = taskList.getElementsByTagName("li");
  return tasks.length;
}